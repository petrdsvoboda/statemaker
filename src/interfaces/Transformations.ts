import Data from './Data'

/** Import interface */
export type ImportFn = (data: string) => Data
/** Export interface */
export type ExportFn = (data: Data) => string
/** Positioning interface */
export type PositionFn = (data: Data) => Data

type ValidateArg<T> = T extends Data ? Data : any
/** Validating interface */
export type ValidateFn<T> = (data: ValidateArg<T>) => string[]
