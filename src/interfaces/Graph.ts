import uuid from 'uuid'

import { getTextSize } from '../utils/textSize'
import Data from './Data'
import Map from './Map'
import Position from './Position'
import State from './State'
import Transition from './Transition'

/**
 * Vertex representing state
 *
 * @export
 * @interface Vertex
 */
export interface Vertex {
	id: string
	name: string
	position: Position
}
/**
 * Directed edge representing transition
 *
 * @export
 * @interface Edge
 */
export interface Edge {
	id: string
	/** Id of state that transition is coming out */
	from: string
	/** Id of state that transition is coming into */
	to: string
	name: string
}

/** Map of vertices */
export type VertexMap<V extends Vertex> = Map<V>

/**
 * Generic graph of vertices and edges
 *
 * @export
 * @interface Graph
 * @template V
 * @template E
 */
export default interface Graph<V extends Vertex, E extends Edge> {
	vertices: VertexMap<V>
	edges: E[]
}

// HELPERS

/**
 * Creates generic element with selected defaults
 *
 * @template T
 * @param {T} defaults Default params for elements
 */
export const createElement = <T>(defaults: T) => (data: Partial<T>): T => ({
	...defaults,
	...data
})
/**
 * Updates generic element with data
 *
 * @template T
 * @param {T} data Update data
 */
export const updateElement = <T>(data: Partial<T>) => (el: T): T => ({
	...el,
	...data
})

/** Base graph vertex */
export const baseVertex = createElement<Vertex>({
	id: uuid.v4(),
	name: 'tempV',
	position: { x: undefined, y: undefined } as any
})({})
/** Base graph edge */
export const baseEdge = createElement<Edge>({
	id: uuid.v4(),
	name: 'tempE',
	from: '',
	to: ''
})({})

/**
 * Convertes array of elements with id to map
 *
 * @template T
 * @param {T[]} arr
 */
export const toMap = <T extends { id: string }>(arr: T[]) =>
	arr.reduce<Map<T>>((acc, curr) => ({ ...acc, [curr.id]: curr }), {})

/**
 * Converts map of elements with id to array
 *
 * @template T
 * @param {Map<T>} map
 */
export const toArray = <T extends { id: string }>(map: Map<T>) =>
	Object.values(map)

/**
 * Maps over map of elements with id
 *
 * @template T
 * @param {(a: T) => T} fn Function applied to mapped element
 */
export const mapMap = <T extends { id: string }>(fn: (a: T) => T) => (
	map: Map<T>
) =>
	toArray(map).reduce<Map<T>>(
		(acc, curr) => ({ ...acc, [curr.id]: fn(curr) }),
		{}
	)

/**
 * Convert states to vertices
 *
 * @template V
 * @param {V} defaultV
 */
const reduceStates = <V extends Vertex>(defaultV: V) => (
	acc: VertexMap<V>,
	curr: State
): VertexMap<V> => ({
	...acc,
	[curr.id]: {
		...defaultV,
		...{
			id: curr.id,
			name: curr.name,
			position: { x: undefined, y: undefined }
		}
	}
})

/**
 * Convert transitions to edges
 *
 * @template E
 * @param {E} defaultE
 */
const mapTransitions = <E extends Edge>(defaultE: E) => (t: Transition): E =>
	({
		...defaultE,
		id: t.id,
		from: t.startState,
		to: t.endState,
		name: t.name
	} as E)

/**
 * Convert data to graph
 * Uses default vertex and transition to supply default params
 *
 * @template V
 * @template E
 * @param {V} defaultV
 * @param {E} defaultE
 */
export const toGraph = <V extends Vertex, E extends Edge>(
	defaultV: V,
	defaultE: E
) => (data: Data): Graph<V, E> => ({
	vertices: Object.values(data.states).reduce(reduceStates(defaultV), {}),
	edges: Object.values(data.transitions).map<E>(mapTransitions(defaultE))
})

/**
 * Convert graph to data
 * Uses original data to supply additional params
 *
 * @template V
 * @template E
 * @param {Data} data Original data
 * @param {Graph<V, E>} graph
 * @returns {Data}
 */
export const toData = <V extends Vertex, E extends Edge>(
	data: Data,
	graph: Graph<V, E>
): Data => ({
	...data,
	states: Object.values(graph.vertices).reduce<Map<State>>(
		(acc, curr) =>
			data.states[curr.id]
				? {
						...acc,
						[curr.id]: {
							...data.states[curr.id],
							position: curr.position,
							size: getTextSize(curr.name)
						} as State
				  }
				: acc,
		{} as Map<State>
	)
})

export type EdgeReducer<T, E extends Edge> = (acc: T, curr: E) => T
export type VertexReducer<T, V extends Vertex> = (acc: T, curr: V) => T
