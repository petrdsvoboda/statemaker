import Data from '../Data'
import TGraph, {
	Vertex as TVertex,
	Edge as TEdge,
	VertexMap as TVertexMap,
	createElement,
	baseVertex,
	baseEdge,
	updateElement,
	toMap,
	toArray,
	mapMap,
	toGraph,
	toData
} from '../Graph'

interface Vertex extends TVertex {
	test: number
}
interface Edge extends TEdge {
	test: number
}
type Graph = TGraph<Vertex, Edge>
type VertexMap = TVertexMap<Vertex>

const vertex = <Vertex>{
	id: 'test',
	name: 'Test',
	position: { x: 0, y: 0 },
	test: 0
}
const defaultVertex: Vertex = { ...baseVertex, name: 'Test', test: 0 }
const defaultEdge: Edge = { ...baseEdge, name: 'Test', test: 0 }
const createVertex = createElement(defaultVertex)
const createEdge = createElement(defaultEdge)

const vertices1: VertexMap = {
	v1: createVertex({ id: 'v1' }),
	v2: createVertex({ id: 'v2' }),
	v3: createVertex({ id: 'v3' })
}

const vertices2: VertexMap = {
	v1: createVertex({ id: 'v1', test: 2 }),
	v2: createVertex({ id: 'v2', test: 2 }),
	v3: createVertex({ id: 'v3', test: 2 })
}

const edges1: Edge[] = [
	createEdge({
		id: 'e1',
		name: 'edge',
		from: 'v1',
		to: 'v2'
	}),
	createEdge({
		id: 'e2',
		name: 'edge',
		from: 'v1',
		to: 'v2'
	})
]
const edges2: Edge[] = [
	createEdge({
		id: 'e1',
		from: 'v1',
		to: 'v2',
		name: 'edge',
		test: 2
	}),
	createEdge({
		id: 'e3',
		from: 'v1',
		to: 'v2',
		name: 'edge',
		test: 2
	})
]

const graph = {
	vertices: vertices1,
	edges: edges1
}

const data: Data = {
	initialStates: ['1'],
	finalStates: [],
	states: {
		v1: {
			id: 'v1',
			name: 'Test',
			position: { x: 0, y: 0 },
			size: 2
		},
		v2: {
			id: 'v2',
			name: 'Test',
			position: { x: 0, y: 0 },
			size: 3
		},
		v3: {
			id: 'v3',
			name: 'Test',
			position: { x: 0, y: 0 },
			size: 3
		}
	},
	transitions: {
		e1: {
			id: 'e1',
			name: 'edge',
			startState: 'v1',
			endState: 'v2'
		},
		e2: {
			id: 'e2',
			name: 'edge',
			startState: 'v1',
			endState: 'v2'
		}
	}
}

test('creates vertex', () => {
	expect(createVertex({ test: 2 })).toEqual({
		...defaultVertex,
		test: 2
	} as Vertex)
})
test('updates vertex', () => {
	expect(updateElement({ position: { x: 10, y: 10 } })(vertex)).toEqual({
		...vertex,
		position: { x: 10, y: 10 }
	} as Vertex)
})
test('converts arr to map', () => {
	expect(toMap([{ id: 'test1' }, { id: 'test2', name: 'Test' }])).toEqual({
		test1: { id: 'test1' },
		test2: { id: 'test2', name: 'Test' }
	})
})
test('converts map to arr', () => {
	expect(
		toArray({
			test1: { id: 'test1' },
			test2: { id: 'test2', name: 'Test' }
		})
	).toEqual([{ id: 'test1' }, { id: 'test2', name: 'Test' }])
})
test('maps over map', () => {
	expect(mapMap<Vertex>(v => ({ ...v, test: 2 }))(vertices1)).toEqual(
		vertices2
	)
})
test('generates graph from data', () => {
	expect(toGraph(defaultVertex, defaultEdge)(data)).toEqual(graph)
})
test('generates data from graph', () => {
	expect(
		toData(data, {
			...graph,
			vertices: {
				...graph.vertices,
				v1: {
					...graph.vertices.v1,
					position: { x: 0, y: 0 }
				},
				v2: {
					...graph.vertices.v2,
					position: { x: 0, y: 0 }
				},
				v3: {
					...graph.vertices.v3,
					position: { x: 0, y: 0 }
				},
				v8: {
					...graph.vertices.v3,
					position: { x: 0, y: 0 }
				}
			}
		})
	).toEqual({
		...data,
		states: {
			...data.states,
			v1: {
				size: 0,
				position: { x: 0, y: 0 },
				id: graph.vertices.v1.id,
				name: graph.vertices.v1.name
			},
			v2: {
				size: 0,
				position: { x: 0, y: 0 },
				id: graph.vertices.v2.id,
				name: graph.vertices.v2.name
			},
			v3: {
				size: 0,
				position: { x: 0, y: 0 },
				id: graph.vertices.v3.id,
				name: graph.vertices.v3.name
			}
		}
	})
})
