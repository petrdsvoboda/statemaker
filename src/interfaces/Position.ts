import { round as roundNum } from '../utils/number'

/**
 * used for storing element positions and positioning methods
 *
 * @export
 * @interface Position
 */
export default interface Position {
	x: number
	y: number
}

type MonoOperator = (a: Position) => Position
type MonoNumOperator = (a: Position) => number
type Operator = (a: Position, b: Position) => Position
type NumOperator = (a: Position, b: number) => Position

// MATH OPERATIONS

/**
 * Adds params of a to b
 *
 * @param {Position} a
 * @param {Position} b
 */
export const add: Operator = (a, b) => ({
	x: a.x + b.x,
	y: a.y + b.y
})
/**
 * Adds x to params of a
 *
 * @param {Position} a
 * @param {number} x
 */
export const addX: NumOperator = (a, x) => ({
	x: a.x + x,
	y: a.y + x
})
/**
 * Subtracts params of b from a
 *
 * @param {Position} a
 * @param {Position} b
 */
export const subtract: Operator = (a, b) => ({
	x: a.x - b.x,
	y: a.y - b.y
})
/**
 * Subtracts x from params of a
 *
 * @param {Position} a
 * @param {number} x
 */
export const subtractX: NumOperator = (a, x) => ({
	x: a.x - x,
	y: a.y - x
})
/**
 * Multiplies params of a and b
 *
 * @param {Position} a
 * @param {Position} b
 */
export const multiply: Operator = (a, b) => ({
	x: a.x * b.x,
	y: a.y * b.y
})
/**
 * Multiplies params of a by x
 *
 * @param {Position} a
 * @param {number} x
 */
export const multiplyByX: NumOperator = (a, x) => ({
	x: a.x * x,
	y: a.y * x
})
/**
 * Divides params of a by b
 * Check for zero division and in that case assigns 0
 *
 * @param {Position} a
 * @param {Position} b
 */
export const divide: Operator = (a, b) => ({
	x: b.x === 0 ? 0 : a.x / b.x,
	y: b.y === 0 ? 0 : a.y / b.y
})
/**
 * Divides params of a by x
 * Check for zero division and in that case assigns 0
 *
 * @param {Position} a
 * @param {number} x
 */
export const divideByX: NumOperator = (a, x) => ({
	x: x === 0 ? 0 : a.x / x,
	y: x === 0 ? 0 : a.y / x
})
/**
 * Gets the modulo x of params of a
 *
 * @param {Position} a
 * @param {number} x
 */
export const modX: NumOperator = (a, x) => ({
	x: a.x % x,
	y: a.y % x
})
/**
 * Gets params of params of a to power pow
 *
 * @param {Position} a
 * @param {number} pow
 */
export const toPower: NumOperator = (a, pow) => ({
	x: Math.pow(a.x, pow),
	y: Math.pow(a.y, pow)
})
/**
 * Gets square root of params of a
 *
 * @param {Position} a
 */
export const squareRoot: MonoOperator = a => ({
	x: Math.sqrt(a.x),
	y: Math.sqrt(a.y)
})
/**
 * Gets absolute value of params of a
 *
 * @param {Position} a
 */
export const absolute: MonoOperator = a => ({
	x: Math.abs(a.x),
	y: Math.abs(a.y)
})
/**
 * Gets min of params of a and x
 *
 * @param {Position} a
 * @param {number} x
 */
export const minX: NumOperator = (a, x) => ({
	x: Math.min(a.x, x),
	y: Math.min(a.y, x)
})
/**
 * Gets min of params of a and b
 *
 * @param {Position} a
 * @param {Position} b
 */
export const min: Operator = (a, b) => ({
	x: Math.min(a.x, b.x),
	y: Math.min(a.y, b.y)
})
/**
 * Gets max of params of a and x
 *
 * @param {Position} a
 * @param {number} x
 */
export const maxX: NumOperator = (a, x) => ({
	x: Math.max(a.x, x),
	y: Math.max(a.y, x)
})
/**
 * Gets max of params of a and b
 *
 * @param {Position} a
 * @param {Position} b
 */
export const max: Operator = (a, b) => ({
	x: Math.max(a.x, b.x),
	y: Math.max(a.y, b.y)
})
/**
 * Gets distance of vector a from (0, 0) to (a.x, a.y)
 *
 * @param {Position} a
 */
export const distance: MonoNumOperator = a =>
	Math.sqrt(Math.pow(a.x, 2) + Math.pow(a.y, 2))

/**
 * Rounds params of a to precision prec
 *
 * @param {Position} a
 * @param {prec} prec
 */
export const round = (a: Position, prec: number = 2): Position => ({
	x: roundNum(a.x, prec),
	y: roundNum(a.y, prec)
})

// HELPERS

/**
 * Creates position (x, y)
 *
 * @param {number} x
 * @param {number} y
 * @returns {Position}
 */
export const toPosition = (x: number, y: number): Position => ({ x, y })

/**
 * Creates random position between bounds
 * Bounds are offset by half from top and left
 *
 * @param {Position} bounds
 * @returns {Position}
 */
export const random: MonoOperator = bounds =>
	add(
		multiply(bounds, toPosition(Math.random(), Math.random())),
		divideByX(bounds, 2)
	)

/**
 * Moves position a between bounds
 * Bounds have empty zone of 0.1 at the edges
 *
 * @param {Position} bounds
 * @returns {Position}
 */
export const toBounds: Operator = (bounds, a) =>
	min(
		subtract(bounds, multiplyByX(bounds, 0.1)),
		max(add(multiplyByX(bounds, 0.1), toPosition(0, 0)), a)
	)

/**
 * Zooms to position to by factor
 * Uses previous scale and offset to calculate current values
 *
 * @param {Position} to
 * @param {Position} offset
 * @param {number} scale
 * @param {number} factor
 * @returns {Position}
 */
export const zoom = (
	to: Position,
	offset: Position,
	scale: number,
	factor: number
): Position => {
	const newScale = scale + factor
	const change = newScale - scale
	return round(add(multiplyByX(to, -change), offset))
}

/**
 * Normalizes position to current scale and offset
 *
 * @param {Position} a
 * @param {Position} offset
 * @param {number} scale
 */
export const normalize = (a: Position, offset: Position, scale: number) =>
	multiplyByX(subtract(a, offset), 1 / scale)

/**
 * Snaps position to the closest point on the grid of size
 *
 * @param {Position} position - Current position
 * @param {number} gridSize - Size of the grid
 * @return {Position} Snapped position
 */
export const snap: NumOperator = (position, gridSize) =>
	multiplyByX(round(divideByX(position, gridSize), 0), gridSize)
