import Map from './Map'
import State from './State'
import Transition from './Transition'

/** State machine state map */
export type StateMap = Map<State>
/** State machine transition map */
export type TransitionMap = Map<Transition>

export default interface Data {
	/** Initial machine states */
	initialStates: string[]
	/** Final machine states */
	finalStates: string[]
	/** State machine state map */
	states: StateMap
	/** State machine transition map */
	transitions: TransitionMap
}
