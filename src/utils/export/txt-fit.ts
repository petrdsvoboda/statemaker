import { ExportFn } from '../../interfaces/Transformations'

interface ExportState {
	initial: boolean
	final: boolean
	transitions: {
		[key: string]: string
	}
}

interface ExportStateMap {
	[key: string]: ExportState
}

interface ExportData {
	type: string
	transitionNames: string[]
	states: ExportStateMap
}

/**
 * Exports data in txt-fit format
 *
 * @param {*} data
 * @returns
 */
const exportData: ExportFn = data => {
	let output: ExportData = {
		type: 'FA', // Text file identifier
		transitionNames: Object.values(data.transitions).reduce(
			(acc, curr) =>
				acc.includes(curr.name) ? acc : [...acc, curr.name],
			[] as string[]
		),
		states: Object.values(data.states).reduce(
			(acc, curr) => ({
				...acc,
				[curr.name]: {
					initial: false,
					final: false,
					transitions: {}
				}
			}),
			{} as ExportStateMap
		)
	}

	// Set initial states
	for (const s of data.initialStates) {
		const state = data.states[s]
		if (!state) continue

		const name = state.name
		output.states[name].initial = true
	}

	// Set final states
	for (const s of data.finalStates) {
		const state = data.states[s]
		if (!state) continue

		const name = state.name
		output.states[name].final = true
	}

	// Set transition
	for (const t of Object.values(data.transitions)) {
		const startState = data.states[t.startState]
		const endState = data.states[t.endState]
		if (!startState || !endState) continue

		const name = startState.name

		const transitions = output.states[name].transitions
		// Split to states by | if there are multiple
		output.states[name].transitions = {
			...transitions,
			[t.name]: transitions[t.name]
				? transitions[t.name] + '|' + endState.name
				: endState.name
		}
	}

	let outputArr: string[][] = [[output.type, ...output.transitionNames]]
	outputArr = [
		...outputArr,
		// Create state rows, add < or > to symbolize initial or final
		...Object.keys(output.states).map(key => {
			const state = output.states[key]
			let name = key
			if (state.final) name = '<' + name
			if (state.initial) name = '>' + name

			return [
				name,
				...output.transitionNames.map(t =>
					state.transitions[t] ? state.transitions[t] : '-'
				)
			]
		})
	]

	return outputArr.map(arr => arr.join('\t')).join('\n')
}

export default exportData
