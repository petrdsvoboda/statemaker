import Data from '../../../interfaces/Data'
import Map from '../../../interfaces/Map'
import State from '../../../interfaces/State'
import Transition from '../../../interfaces/Transition'

import exportData from '../json-xstate'

type StateMap = Map<State>
type TransitionMap = Map<Transition>

const state1: State = {
	id: '1',
	name: 's1',
	position: { x: 0, y: 0 },
	size: 0
}
const state2: State = {
	id: '2',
	name: 's2',
	position: { x: 0, y: 0 },
	size: 0
}
const state3: State = {
	id: '3',
	name: 's3',
	position: { x: 0, y: 0 },
	size: 0
}
const state4: State = {
	id: '4',
	name: 's4',
	position: { x: 0, y: 0 },
	size: 0
}
const stateMap: StateMap = {
	1: state1,
	2: state2,
	3: state3,
	4: state4
}

const transition1: Transition = {
	id: '5',
	name: 't1',
	startState: '1',
	endState: '2'
}
const transition2: Transition = {
	id: '6',
	name: 't2',
	startState: '1',
	endState: '1'
}
const transition3: Transition = {
	id: '7',
	name: 't3',
	startState: '2',
	endState: '3'
}
const transition4: Transition = {
	id: '8',
	name: 't4',
	startState: '4',
	endState: '1'
}
const transitionMap: TransitionMap = {
	5: transition1,
	6: transition2,
	7: transition3,
	8: transition4
}

export const data: Data = {
	states: stateMap,
	transitions: transitionMap,
	initialStates: ['1'],
	finalStates: ['2']
}

export const file: string = `{
    \"id\": "statemaker",
    \"initial\": "s1",
    \"states\": {
        \"s1\": {
            "on": {
                \"t1\": \"s2\",
                \"t2\": \"s1\"
            }
        },
        \"s2\": {
            "on": {
                \"t3\": \"s3\"
            }
        },
        \"s3\": {
            "on": {}
        },
        \"s4\": {
            "on": {
                \"t4\": \"s1\"
            }
        }
    }
}`

export const file2: string = `{
    \"id\": "statemaker",
    \"initial\": "",
    \"states\": {
        \"s1\": {
            "on": {
                \"t1\": \"s2\",
                \"t2\": \"s1\"
            }
        },
        \"s2\": {
            "on": {
                \"t3\": \"s3\"
            }
        },
        \"s3\": {
            "on": {}
        },
        \"s4\": {
            "on": {
                \"t4\": \"s1\"
            }
        }
    }
}`

test('expect convert data to json-xstate', () => {
	expect(exportData(data)).toEqual(file)
	expect(exportData({ ...data, initialStates: [] })).toEqual(file2)
})
