import { ExportFn } from '../../interfaces/Transformations'

interface ExportData {
	id: string
	initial: string
	states: {
		[key: string]: {
			on: { [key: string]: string }
		}
	}
}

/**
 * Exports data in json-xtstate format
 * Allows only one initial state and no final ones
 *
 * @param {*} data
 * @returns
 */
const exportData: ExportFn = data => {
	// Set initial state if there is some
	let output: ExportData = {
		id: 'statemaker',
		initial:
			data.initialStates[0] && data.states[data.initialStates[0]]
				? data.states[data.initialStates[0]].name
				: '',
		states: {}
	}
	// Create states
	output = Object.values(data.states).reduce(
		(o, s) => ({
			...o,
			states: {
				...o.states,
				[s.name]: { on: {} }
			}
		}),
		output
	)
	// Crate transitions
	output = Object.values(data.transitions).reduce((o, t) => {
		const startState = data.states[t.startState]
		const endState = data.states[t.endState]
		return {
			...o,
			states: {
				...o.states,
				[startState.name]: {
					on: {
						...o.states[startState.name].on,
						[t.name]: endState.name
					}
				}
			}
		}
	}, output)

	return JSON.stringify(output, null, 4)
}

export default exportData
