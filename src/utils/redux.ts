type FunctionType = (...args: any[]) => any
/**
 *  Map of action createor ids and its actions
 *
 * @interface ActionCreatorsMapObject
 */
interface ActionCreatorsMapObject {
	[actionCreator: string]: FunctionType
}

/** Union all action into one type */
export type ActionsUnion<A extends ActionCreatorsMapObject> = ReturnType<
	A[keyof A]
>

/**
 * Base redux action of type T
 *
 * @interface Action
 * @template T
 */
interface Action<T extends string> {
	type: T
}

/**
 * Payloaded redux action of type T and payload P
 *
 * @interface Action
 * @template T
 */
interface ActionWithPayload<T extends string, P> extends Action<T> {
	payload: P
}

/**
 * Base action creator of action
 *
 * @export
 * @template T
 * @param {T} type
 * @returns {Action<T>}
 */
export function createAction<T extends string>(type: T): Action<T>

/**
 * Payload action creator of action
 *
 * @export
 * @template T
 * @param {T} type
 * @param {P} payload
 * @returns {Action<T>}
 */
export function createAction<T extends string, P>(
	type: T,
	payload: P
): ActionWithPayload<T, P>
/**
 * Payload action creator of action
 *
 * @export
 * @template T
 * @template P
 * @param {T} type
 * @param {P} [payload]
 * @returns
 */
export function createAction<T extends string, P>(type: T, payload?: P) {
	return payload === undefined ? { type } : { type, payload }
}
