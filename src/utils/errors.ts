import IState from '../interfaces/State'
import ITransition from '../interfaces/Transition'
import Data from '../interfaces/Data'
import { State as Canvas } from '../reducers/canvas'

interface DataMap<T> {
	[key: string]: T
}

/**
 * Check if element has empty name
 *
 * @param {{ name: string }} el
 * @returns
 */
function hasNoName(el: { name: string }) {
	return el.name === ''
}

/**
 * Check if there is state with same name in state map
 *
 * @param {DataMap<IState>} states
 * @param {IState} state
 * @returns
 */
function hasSameName(states: DataMap<IState>, state: IState) {
	return Object.values(states).some(
		s => s.name === state.name && s.id !== state.id
	)
}

/**
 * Check for all state errors
 *
 * @export
 * @param {DataMap<IState>} states
 * @param {IState} [state]
 * @returns
 */
export function hasStateError(states: DataMap<IState>, state?: IState) {
	return state !== undefined
		? hasNoName(state) || hasSameName(states, state)
		: false
}

/**
 * Check if there is transition from and to same states with same name
 *
 * @param {DataMap<ITransition>} transitions
 * @param {ITransition} transition
 * @returns
 */
function hasSameNameTransition(
	transitions: DataMap<ITransition>,
	transition: ITransition
) {
	return Object.values(transitions).some(
		t =>
			t.name === transition.name &&
			t.id !== transition.id &&
			t.startState === transition.startState &&
			t.endState === transition.endState
	)
}

/**
 * Check for all transition errors
 *
 * @export
 * @param {DataMap<ITransition>} transitions
 * @param {ITransition} [transition]
 * @returns
 */
export function hasTransitionError(
	transitions: DataMap<ITransition>,
	transition?: ITransition
) {
	return transition !== undefined
		? hasNoName(transition) ||
				hasSameNameTransition(transitions, transition)
		: false
}

/**
 * Show hasNoName errors
 *
 * @param {DataMap<{ id: string; name: string }>} map
 * @param {string} shownId
 * @param {string} name
 * @returns
 */
function noNameReducer(
	map: DataMap<{ id: string; name: string }>,
	shownId: string,
	name: string
) {
	return Object.values(map).reduce(
		(e, s) =>
			hasNoName(s) && s.id !== shownId
				? [...e, name + ' has no name']
				: e,
		[] as string[]
	)
}

/**
 * Show hasSameName state errors
 *
 * @param {DataMap<IState>} map
 * @param {string} shownId
 * @returns
 */
function sameNameReducer(map: DataMap<IState>, shownId: string) {
	return Object.values(map).reduce(
		(e, s) => {
			const error = 'Multiple states with name: ' + s.name
			return hasSameName(map, s) && !e.includes(error) && s.id !== shownId
				? [...e, error]
				: e
		},
		[] as string[]
	)
}

/**
 * Show hasSameNameTransition errors
 * Create only one error if there is hasSameNameTransition error
 *
 * @param {DataMap<ITransition>} map
 * @param {string} shownId
 * @returns
 */
function sameNameTransitionReducer(map: DataMap<ITransition>, shownId: string) {
	return Object.values(map).reduce(
		(e, t) => {
			const error = 'Multiple transitions from state with name: ' + t.name
			return hasSameNameTransition(map, t) &&
				!e.includes(error) &&
				t.id !== shownId
				? [...e, error]
				: e
		},
		[] as string[]
	)
}

/**
 * Check for all errors in constructed state machine
 *
 * @export
 * @param {Data} state
 * @param {Canvas} canvas
 * @returns {string[]}
 */
export function getErrors(state: Data, canvas: Canvas): string[] {
	let errors: string[] = []

	errors = [
		...errors,
		...noNameReducer(state.states, canvas.selectedState, 'State')
	]
	errors = [
		...errors,
		...noNameReducer(
			state.transitions,
			canvas.selectedTransition,
			'Transition'
		)
	]
	errors = [...errors, ...sameNameReducer(state.states, canvas.selectedState)]
	errors = [
		...errors,
		...sameNameTransitionReducer(
			state.transitions,
			canvas.selectedTransition
		)
	]

	return errors
}
