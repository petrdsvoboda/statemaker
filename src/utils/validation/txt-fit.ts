import { validateTypesName, validateStringArr } from '../validation'
import { ValidateFn } from '../../interfaces/Transformations'

/**
 * Validate txt-fit file
 *
 * @param {*} data
 * @returns
 */
const validate: ValidateFn<any> = data => {
	let errors: string[] = []

	if (data.length === 0) {
		return [...errors, 'Missing data lines']
	}

	const id = data[0][0]
	// Check for file identifier
	if (id !== 'FA') {
		return [...errors, 'Data identifier type is not FA']
	}

	const transitionNames = data[0].slice(1)
	const transitionCount = transitionNames.length
	const states = data.slice(1)

	errors = [
		...errors,
		...validateTypesName(transitionNames),
		...validateStringArr('State')(states, transitionCount + 1)
	]

	return errors
}

export default validate
