import Data from '../../../interfaces/Data'

import { loadData } from '../'

describe('import', () => {
	afterEach(() => {
		jest.resetModules()
	})
	beforeEach(() => {
		jest.mock('uuid')
	})

	it('loads txt data', () => {
		loadData('s\n\n', 'txt')
	})

	it('loads txt-fit data', () => {
		loadData('FA', 'txt')
	})

	it('loads json-xstate data', () => {
		loadData('{"id":"xstate","states":{}, "initial":""}', 'json')
	})

	it('loads json data', () => {
		loadData('{"states":{}, "initial":[], "final":[]}', 'json')
	})

	it('does not load wrong format', () => {
		expect(loadData('', 'xxx')).toEqual({
			errors: ['Imported file has wrong extension']
		})
	})

	it('does not load corrupted format', () => {
		expect(loadData('sdhfuisdfgsdf', 'json')).toEqual({
			errors: ['Error while importing data']
		})
	})
})
