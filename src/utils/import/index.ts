import Data from '../../interfaces/Data'
import FileVariant from '../../interfaces/FileVariant'
import { ImportFn } from '../../interfaces/Transformations'
import JSONBASIC from './json'
import JSONXSTATE from './json-xstate'
import TXT from './txt'
import TXTFIT from './txt-fit'

import validateData from '../validation/data'
import validateJSONBASIC from '../validation/json'
import validateJSONXSTATE from '../validation/json-xstate'
import validateTXT from '../validation/txt'
import validateTXTFIT from '../validation/txt-fit'

type VariantData = { variant: FileVariant; errors: string[] }

/**
 * Decide the file variant and check for specific file errors
 *
 * @param {string} data
 * @param {string} filetype
 * @returns {VariantData}
 */
const getVariant = (data: string, filetype: string): VariantData => {
	let variant: FileVariant = 'json'
	let errors: string[] = []

	if (filetype === 'json') {
		const obj = JSON.parse(data)

		// Xstate has specific key - id: json-xstate
		if (obj.hasOwnProperty('id')) {
			variant = 'json-xstate'
			errors = [...errors, ...validateJSONXSTATE(obj)]
		} else {
			variant = 'json'
			errors = [...errors, ...validateJSONBASIC(obj)]
		}
	} else if (filetype === 'txt') {
		const txtType = data.slice(0, 2)
		const arr = data.split('\n').map(l => l.split('\t'))

		// fit has specific first two letter - FA
		if (txtType === 'FA') {
			variant = 'txt-fit'
			errors = [...errors, ...validateTXTFIT(arr)]
		} else {
			variant = 'txt'
			errors = [...errors, ...validateTXT(arr)]
		}
	} else {
		errors = [...errors, 'Imported file has wrong extension']
	}

	return {
		variant,
		errors
	}
}

const ImportTypeMap: { [key in FileVariant]: ImportFn } = {
	json: JSONBASIC,
	'json-xstate': JSONXSTATE,
	txt: TXT,
	'txt-fit': TXTFIT
}

type ReturnType = { data?: Data; errors?: string[] }
/**
 * Loads data from string, decides the file variant and converts it to data
 * Checks for errors and returns error array if there are errors
 *
 * @export
 * @param {string} data
 * @param {string} filetype
 * @returns {ReturnType}
 */
export function loadData(data: string, filetype: string): ReturnType {
	let variantData: VariantData
	try {
		variantData = getVariant(data, filetype)
	} catch {
		return {
			errors: ['Error while importing data']
		}
	}

	// No need to continue if there are errors
	if (variantData.errors.length > 0) {
		return { errors: variantData.errors }
	}

	// Find the right import function
	const importType = ImportTypeMap[variantData.variant]
	const importedData = importType(data)

	// Validate state machine
	const errors = [...variantData.errors, ...validateData(importedData)]

	return { data: importedData, errors }
}
