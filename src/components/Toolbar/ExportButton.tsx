import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import ExportIcon from '@material-ui/icons/CloudDownload'

import { Actions, State as ReduxState } from '../../reducers'
import { downloadData, Variant as ExportVariant } from '../../utils/export'
import MenuButton from './MenuButton'

/**
 * Manages exporting of current state machine
 *
 * @returns
 */
function ExportButton() {
	const actions = useActions(Actions, [])
	const { data } = useSelector(
		(state: ReduxState) => ({
			data: state.data.present
		}),
		[]
	)

	const handleExport = React.useCallback(
		(variant: ExportVariant) => () => {
			// Text files can't have line breaks in element names
			if (
				(variant === 'txt' || variant === 'txt-fit') &&
				[
					...Object.values(data.states).map(s => s.name),
					...Object.values(data.transitions).map(t => t.name)
				].reduce(
					(acc, curr) => (acc ? acc : curr.includes('\n')),
					false
				)
			) {
				actions.addError(
					"Can't export to text files when element names have line breaks"
				)
			} else {
				downloadData(data, variant)
			}
		},
		[actions, data]
	)

	return (
		<MenuButton
			Icon={ExportIcon}
			label="Export to file"
			selected={false}
			menuItems={[
				{
					label: 'json',
					onAction: handleExport('json')
				},
				{
					label: 'json - xstate',
					onAction: handleExport('json-xstate')
				},
				{
					label: 'txt',
					onAction: handleExport('txt')
				},
				{
					label: 'txt - fit',
					onAction: handleExport('txt-fit')
				}
			]}
		/>
	)
}

export default ExportButton
