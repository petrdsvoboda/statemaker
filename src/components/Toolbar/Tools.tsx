import * as React from 'react'
import { batch, useActions, useSelector, useDispatch } from 'react-redux'
import { ActionCreators } from 'redux-undo'

import Paper from '@material-ui/core/Paper'
import { Theme } from '@material-ui/core/styles'
import HelpIcon from '@material-ui/icons/HelpOutline'
import LoadIcon from '@material-ui/icons/FolderOpen'
import ResetIcon from '@material-ui/icons/Clear'
import ZoomInIcon from '@material-ui/icons/ZoomIn'
import ZoomOutIcon from '@material-ui/icons/ZoomOut'
import MoveIcon from '@material-ui/icons/OpenWith'
import SelectIcon from '@material-ui/icons/TouchApp'
import UndoIcon from '@material-ui/icons/Undo'
import RedoIcon from '@material-ui/icons/Redo'
import SplashIcon from '@material-ui/icons/Explore'
import GridIcon from '@material-ui/icons/GridOn'
import { makeStyles } from '@material-ui/styles'

import { Actions, State as ReduxState } from '../../reducers'
import { loadState } from '../../utils/persist'
import ElementButton from './ElementButton'
import StateButton from './StateButton'
import TransitionButton from './TransitionButton'
import PositionButton from './PositionButton'
import SetAlgorithmButton from './SetAlgorithmButton'
import ImportButton from './ImportButton'
import ExportButton from './ExportButton'
import SaveButton from './SaveButton'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		maxHeight: '100%',
		overflowY: 'auto' as 'auto',
		paddingRight: 8,
		'& > *': {
			marginLeft: 8,
			marginTop: 8,
			display: 'flex',
			flexDirection: 'column' as 'column',
			overflow: 'hidden',
			'& > button': {
				borderRadius: 0,
				color: 'rgba(0, 0, 0, 0.54)',
				'&:hover': {
					color: theme.palette.primary.light,
					backgroundColor: 'transparent'
				},
				'&[data-selected="true"]': {
					color: theme.palette.primary.main
				}
			}
		},
		'& > *:first-child': {},
		'& > *:last-child': {
			marginBottom: 8
		}
	},
	main: {
		backgroundColor: theme.palette.primary.main,
		'& > button': {
			color: 'rgba(255, 255, 255, 0.84)',
			'&:hover': {
				color: theme.palette.primary.contrastText
			},
			'&[data-selected="true"]': {
				color: 'rgba(255, 255, 255, 0.84)',
				backgroundColor: theme.palette.primary.light
			}
		}
	}
}))

/**
 * Panels with buttons from the main toolbar
 *
 * @returns
 */
function Tools() {
	const actions = useActions(Actions, [])
	const dispatch = useDispatch()
	const classes = useStyles()
	const { cursorMode, grid } = useSelector(
		(state: ReduxState) => ({
			cursorMode: state.canvas.cursorMode,
			grid: state.toolbar.grid
		}),
		[]
	)

	const handleSetCursorMode = React.useCallback(
		(mode: 'select' | 'move') => () => {
			actions.setCursorMode(mode)
		},
		[actions]
	)

	const handleZoomIn = React.useCallback(() => {
		actions.zoom(0.25)
	}, [actions])

	const handleZoomOut = React.useCallback(() => {
		actions.zoom(-0.25)
	}, [actions])

	const handleLoad = React.useCallback(() => {
		const data = loadState()
		if (data) {
			batch(() => {
				actions.set(data)
				actions.setConfirmText('Automata loaded')
			})
		}
	}, [actions])

	const handleReset = React.useCallback(() => {
		batch(() => {
			actions.delete()
			actions.deselect()
		})
	}, [actions])

	const handleToggleGrid = React.useCallback(() => {
		actions.toggleGrid()
	}, [actions])

	const handleToggleHint = React.useCallback(() => {
		actions.toggleShowHint()
	}, [actions])

	const handleToggleExplore = React.useCallback(() => {
		actions.setShowExplore(true)
	}, [actions])

	const handleUndo = React.useCallback(() => {
		dispatch(ActionCreators.undo())
	}, [dispatch])

	const handleRedo = React.useCallback(() => {
		dispatch(ActionCreators.redo())
	}, [dispatch])

	return (
		<div className={classes.root}>
			<Paper className={classes.main}>
				<StateButton variant="default" />
				<StateButton variant="initial" />
				<StateButton variant="final" />
				<TransitionButton />
			</Paper>
			<Paper>
				<ElementButton
					Icon={SelectIcon}
					label="Select mode"
					selected={cursorMode === 'select'}
					onAction={handleSetCursorMode('select')}
				/>
				<ElementButton
					Icon={MoveIcon}
					label="Move mode"
					selected={cursorMode === 'move'}
					onAction={handleSetCursorMode('move')}
				/>
				<ElementButton
					Icon={ZoomInIcon}
					label="Zoom in"
					selected={false}
					onAction={handleZoomIn}
				/>
				<ElementButton
					Icon={ZoomOutIcon}
					label="Zoom out"
					selected={false}
					onAction={handleZoomOut}
				/>
				<ElementButton
					Icon={GridIcon}
					label="Toggle grid"
					selected={grid}
					onAction={handleToggleGrid}
				/>
			</Paper>
			<Paper>
				<PositionButton />
				<SetAlgorithmButton />
			</Paper>
			<Paper>
				<ElementButton
					Icon={UndoIcon}
					label="Undo"
					onAction={handleUndo}
				/>
				<ElementButton
					Icon={RedoIcon}
					label="Redo"
					onAction={handleRedo}
				/>
				<SaveButton />
				<ElementButton
					Icon={LoadIcon}
					label="Load from memory"
					selected={false}
					onAction={handleLoad}
				/>
				<ElementButton
					Icon={ResetIcon}
					label="Reset canvas"
					selected={false}
					onAction={handleReset}
				/>
			</Paper>
			<Paper>
				<ExportButton />
				<ImportButton />
			</Paper>
			<Paper>
				<ElementButton
					Icon={HelpIcon}
					label="Show hint"
					selected={false}
					onAction={handleToggleHint}
				/>
				<ElementButton
					Icon={SplashIcon}
					label="Show explore screen"
					onAction={handleToggleExplore}
				/>
			</Paper>
		</div>
	)
}

export default Tools
