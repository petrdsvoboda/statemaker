import * as React from 'react'

import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import { makeStyles } from '@material-ui/styles'

type HandlerKey = 'onMouseDown' | 'onClick'
type Handler = {
	[key in HandlerKey]?: (e: React.MouseEvent<HTMLButtonElement>) => void
}

export interface Props {
	/** Icon shown on button */
	Icon: React.ComponentType
	/** Tooltip text */
	label: string
	selected?: boolean
	isDisabled?: boolean
	/** Triggered on event */
	onAction?: (e: React.MouseEvent<HTMLButtonElement>) => void
	/** What event gets triggered */
	event?: 'onClick' | 'onMouseDown'
	className?: string
}

const useStyles = makeStyles({
	root: {
		padding: 8
	}
})

/**
 * Toolbar base button
 *
 * @param {Props} props
 * @returns
 */
function ElementButton(props: Props) {
	const { Icon, label, onAction, event, className, isDisabled } = props
	const classes = useStyles()

	const handleAction = React.useCallback(
		(e: React.MouseEvent<HTMLButtonElement>) => {
			e.stopPropagation()
			e.preventDefault()
			if (onAction) {
				onAction(e)
			}
		},
		[onAction]
	)

	// Assign the right event handler
	const handler: Handler = {
		[event === 'onMouseDown' ? 'onMouseDown' : 'onClick']: handleAction
	}

	const customClassName = className + ' ' + classes.root
	return (
		<Tooltip title={label} placement="right">
			<IconButton
				className={customClassName}
				color="inherit"
				disableRipple={true}
				disableTouchRipple={true}
				data-selected={props.selected}
				disabled={isDisabled}
				{...handler}
			>
				<Icon />
			</IconButton>
		</Tooltip>
	)
}

export default React.memo(
	ElementButton,
	(prev, next) =>
		prev.selected === next.selected && prev.onAction === next.onAction
)
