import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import ImportIcon from '@material-ui/icons/CloudUpload'

import { Actions, State as ReduxState } from '../../reducers'
import { loadData } from '../../utils/import'
import positionForce from '../../utils/positioning/force'
import positionLayer from '../../utils/positioning/layered'
import ElementButton from './ElementButton'

/**
 * Manages importing state machine from file
 *
 * @returns
 */
function ImportButton() {
	const actions = useActions(Actions, [])
	// Reference to hidden file input element
	const fileInput = React.createRef<HTMLInputElement>()
	const { algorithm } = useSelector(
		(state: ReduxState) => ({
			algorithm: state.toolbar.algorithm
		}),
		[]
	)

	// Click on hidden file input
	const handleImport = React.useCallback(() => {
		if (fileInput && fileInput.current) {
			fileInput.current.click()
		}
	}, [fileInput])

	// Loads selected file and imports
	const handleLoadFile = React.useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => {
			const files = e.target.files
			// Allow anly loading 1 file at once
			if (!files || files.length !== 1) return
			const file = files[0]
			const reader = new FileReader()

			// Split file format
			const filename = file.name.split('.')

			reader.addEventListener('loadend', () => {
				const dataString = reader.result as string
				// Loads and converts data to JSON
				const result = loadData(
					dataString,
					filename[filename.length - 1]
				)
				let chosenPosition = positionLayer
				if (algorithm === 'force') chosenPosition = positionForce

				// If there was error during load, notify user
				if (result.errors && result.errors.length > 0) {
					actions.setErrors(result.errors)
				} else if (result.data) {
					// Otherwise position data and show them
					actions.set(chosenPosition(result.data))
					actions.setConfirmText('Automata imported')
				}
			})
			reader.readAsText(file, 'utf-8')
			e.target.value = ''
		},
		[actions, algorithm]
	)

	return (
		<React.Fragment>
			<input
				ref={fileInput}
				type="file"
				accept="application/json, text/plain, .json, .txt"
				onChange={handleLoadFile}
				style={{
					position: 'absolute',
					display: 'none'
				}}
			/>
			<ElementButton
				Icon={ImportIcon}
				label="Import from file"
				onAction={handleImport}
			/>
		</React.Fragment>
	)
}

export default ImportButton
