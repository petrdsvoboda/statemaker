import * as React from 'react'

import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { makeStyles } from '@material-ui/styles'

import ElementButton, { Props as ElementButtonProps } from './ElementButton'

const useStyles = makeStyles({
	root: {
		minWidth: 128
	}
})

/**
 * Clickable menu item
 *
 * @interface IMenuItem
 */
interface IMenuItem {
	/** Label text */
	label: string
	selected?: boolean
	/** Triggered on mouse event */
	onAction: () => void
}

interface Props extends ElementButtonProps {
	/** What menu items to show */
	menuItems: IMenuItem[]
}

function MenuButton(props: Props) {
	const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null)
	const classes = useStyles()
	const { menuItems } = props

	// Open menu
	const handleOpen = React.useCallback(
		(event: React.MouseEvent<HTMLButtonElement>) => {
			setAnchorEl(event.currentTarget)
		},
		[]
	)

	// Close menu
	const handleClose = React.useCallback(() => {
		setAnchorEl(null)
	}, [])

	// Dispatch menu item action
	const handleAction = React.useCallback(
		(action: () => void) => () => {
			action()
			setAnchorEl(null)
		},
		[]
	)

	const mapMenuItem = React.useCallback(
		(i: IMenuItem) => (
			<MenuItem
				key={i.label}
				onClick={handleAction(i.onAction)}
				dense={true}
				selected={i.selected}
			>
				{i.label}
			</MenuItem>
		),
		[handleAction]
	)

	return (
		<React.Fragment>
			<ElementButton onAction={handleOpen} {...props} />
			<Menu
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={handleClose}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right'
				}}
				classes={{
					paper: classes.root
				}}
			>
				{menuItems.map(mapMenuItem)}
			</Menu>
		</React.Fragment>
	)
}

export default MenuButton
