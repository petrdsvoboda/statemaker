import * as React from 'react'

import { makeStyles } from '@material-ui/styles'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import { Theme } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		backgroundColor: theme.palette.error.main
	},
	close: {
		padding: 4
	}
}))

interface Props {
	errors: string[]
}

/**
 * Shows error snackbar with error text
 *
 * @param {Props} props
 * @returns
 */
function Error(props: Props) {
	const [open, setOpen] = React.useState(true)
	const classes = useStyles()
	const { errors } = props

	// Close snackbar on close button click
	const handleClose = React.useCallback(
		(_: React.MouseEvent<HTMLElement>, reason?: string) => {
			if (reason === 'clickaway') return
			setOpen(false)
		},
		[]
	)

	// Open when there are errors
	React.useEffect(() => {
		if (errors.length !== 0) setOpen(true)
	}, [errors])

	// Show multiple errors
	const mapContent = React.useCallback(
		(e: string, id: number) => <div key={id}>{e}</div>,
		[]
	)

	return (
		<Snackbar
			anchorOrigin={{
				vertical: 'top',
				horizontal: 'center'
			}}
			open={open}
			onClose={handleClose as any}
		>
			<SnackbarContent
				className={classes.root}
				message={errors.map(mapContent)}
				action={
					<IconButton
						color="inherit"
						className={classes.close}
						onClick={handleClose}
					>
						<CloseIcon />
					</IconButton>
				}
			/>
		</Snackbar>
	)
}

export default React.memo(
	Error,
	(prevProps, nextProps) =>
		prevProps.errors.toString() === nextProps.errors.toString()
)
