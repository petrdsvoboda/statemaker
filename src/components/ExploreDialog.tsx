import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import SvgIcon from '@material-ui/core/SvgIcon'
import { Theme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/styles'
import Transition from '@material-ui/icons/ArrowRightAlt'
import Position from '@material-ui/icons/GpsFixed'
import Algorithm from '@material-ui/icons/Settings'
import Save from '@material-ui/icons/Save'
import Load from '@material-ui/icons/FolderOpen'
import Import from '@material-ui/icons/CloudUpload'
import Export from '@material-ui/icons/CloudDownload'

import { ReactComponent as StateIcon } from '../icons/state.svg'
import { ReactComponent as InitialStateIcon } from '../icons/state_initial.svg'
import { ReactComponent as FinalStateIcon } from '../icons/state_final.svg'
import { Actions, State as ReduxState } from '../reducers'
import { getExploreValue, toggleExplore } from '../utils/persist'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		'& > *': {
			marginBottom: 16
		},
		'& > *:last-child': {
			marginBottom: 0
		},
		'& > p > svg': {
			verticalAlign: 'bottom'
		}
	},
	actions: {
		justifyContent: 'space-between',
		'& > *:first-child': {
			marginLeft: 8
		}
	}
}))

const State = () => (
	<SvgIcon>
		<StateIcon />
	</SvgIcon>
)
const InitialState = () => (
	<SvgIcon>
		<InitialStateIcon />
	</SvgIcon>
)
const FinalState = () => (
	<SvgIcon>
		<FinalStateIcon />
	</SvgIcon>
)

/**
 * Shows explore screen with onfo on app startup
 *
 * @returns
 */
function ExploreDialog() {
	// Check if user previosly chose to hide explore
	const showStorage = React.useMemo(() => getExploreValue(), [])
	// Handles opening explore
	const [toggle, setToggle] = React.useState(showStorage)
	const actions = useActions(Actions, [])
	const { show } = useSelector(
		(state: ReduxState) => ({
			show: state.toolbar.showExplore,
			data: state.data.present
		}),
		[]
	)
	const classes = useStyles()

	// Auto opens explore
	React.useEffect(() => {
		actions.setShowExplore(showStorage)
	}, [actions, showStorage])

	// Hide explore
	const handleClose = React.useCallback(() => {
		actions.setShowExplore(false)
	}, [actions])

	// Disable explore on startup
	const handleDisable = React.useCallback(() => {
		setToggle(toggleExplore())
	}, [])

	return (
		<Dialog open={show} onClose={handleClose}>
			<DialogTitle>Welcome to statemaker</DialogTitle>
			<DialogContent className={classes.root}>
				<DialogContentText>
					Statemaker allows you to draw state machine easily with drag
					and drop.
				</DialogContentText>
				<Divider />
				<DialogContentText>
					Select elements from the toolbar and drag them onto the
					canvas.
				</DialogContentText>
				<DialogContentText>
					You can build states {<State />}, initial states{' '}
					{<InitialState />} and also final states {<FinalState />}.
				</DialogContentText>
				<DialogContentText>
					To draw a transition {<Transition />} between states hover
					over the start state and drag it onto the end state. Or
					choose Add transition from the toolbar.
				</DialogContentText>
				<Divider />
				<DialogContentText>
					You can automatically position {<Position />} created
					machine with multiple different algorithms from the
					selection {<Algorithm />}.
				</DialogContentText>
				<Divider />
				<DialogContentText>
					You can also save {<Save />} and load {<Load />} machine to
					the browser memory.
				</DialogContentText>
				<DialogContentText>
					Or you can import {<Import />} and export {<Export />}{' '}
					machine using multiple file formats.
				</DialogContentText>
				<Divider />
				<DialogContentText>
					There are keybindings to help you with your drawing. The
					list is displayed on the bottom right corner of the screen.
				</DialogContentText>
			</DialogContent>
			<DialogActions className={classes.actions}>
				<FormControlLabel
					control={
						<Checkbox
							onClick={handleDisable}
							checked={toggle}
							color="default"
						/>
					}
					label="Show at startup"
				/>
				<Button onClick={handleClose} color="primary">
					Close
				</Button>
			</DialogActions>
		</Dialog>
	)
}

export default ExploreDialog
