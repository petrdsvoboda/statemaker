import * as React from 'react'
import { batch, useActions, useSelector } from 'react-redux'

import usePosition from '../../hooks/usePosition'
import useTextSize from '../../hooks/useTextSize'

import { Actions, State as ReduxState } from '../../reducers'
import State from './State'

/**
 * Ghost state shown when drawing new state on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
function GhostStateContainer() {
	const actions = useActions(Actions, [])

	const { stateNameId, show, data, type } = useSelector(
		(state: ReduxState) => ({
			stateNameId: state.canvas.stateNameId,
			show: state.toolbar.buildState,
			data: state.canvas.ghostState,
			type: state.toolbar.buildStateType || 'default'
		}),
		[]
	)

	const position = usePosition(show)
	const textRef = useTextSize(data.name, actions.updateGhostState)

	// Create new state on cursor and increase name auto generator counter
	const handleMouseUp = React.useCallback(() => {
		batch(() => {
			actions.addState(
				{
					name: data.name + stateNameId,
					position,
					size: data.size
				},
				type
			)

			actions.useStateNameId()
			actions.setBuildState(false)
		})
	}, [actions, position, stateNameId, type, data.name, data.size])

	if (!show) return null

	return (
		<State
			ref={textRef}
			data={{
				...data,
				name: data.name + stateNameId,
				position
			}}
			isInitial={type === 'initial'}
			isFinal={type === 'final'}
			isGhost={true}
			onMouseUp={handleMouseUp}
		/>
	)
}

export default GhostStateContainer
