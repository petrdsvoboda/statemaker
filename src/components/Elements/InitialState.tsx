import React from 'react'

import Position, {
	round,
	subtract,
	toPosition
} from '../../interfaces/Position'
import { line } from '../../utils/svg'

// Where the initial arrow starts on x
const BEGIN = -50
// Where the initial arrow ends on x
const END = -4

interface Props {
	/** Parent state's id */
	id: string
	/** Show the element */
	show: boolean
	/** Position of base state */
	position: Position
	/** Size of base state */
	size: number
}

/**
 * Represents the arrow pointing to the initial state
 *
 * @param {Props} props
 * @returns
 */
function InitialState(props: Props) {
	const { id, show, position, size } = props

	if (!show) return null

	const markerId = 'initial_' + id

	const begin = toPosition(size - BEGIN, 0)
	const end = toPosition(size - END, 0)

	// Draw line from start of arrow to the end
	const path = line(subtract(position, begin), subtract(position, end))
	const edgePosition = round(subtract(position, begin))
	return (
		<g id="initial">
			<marker
				id={markerId}
				orient="auto"
				markerWidth="3"
				markerHeight="6"
				refX="3"
				refY="3"
			>
				<path d="M0,0 V6 L3,3 Z" />
			</marker>
			<circle r={8} cx={edgePosition.x} cy={edgePosition.y} />
			<path d={path} markerEnd={`url(#${markerId})`} />
		</g>
	)
}

export default InitialState
