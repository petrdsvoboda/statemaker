import React from 'react'
import { batch, useActions, useSelector } from 'react-redux'

import { Theme } from '@material-ui/core/styles'
import TransitionIcon from '@material-ui/icons/ArrowRightAlt'
import { makeStyles } from '@material-ui/styles'

import Position, {
	round,
	subtract,
	toPosition
} from '../../interfaces/Position'
import { Actions, State as ReduxState } from '../../reducers'

interface Props {
	/** Id of state */
	id: string
	/** Overlay should be shown */
	show: boolean
	/** State's position */
	position: Position
	/** State's size */
	size: number
}

const noneString: 'none' = 'none' // Used for correct linting
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		display: 'none',
		'& > #overlay_shadow': {
			display: 'none',
			pointerEvents: noneString
		},
		'&:hover > #overlay_shadow': {
			display: 'block',
			fill: theme.palette.text.disabled
		}
	},
	transition: {
		strokeWidth: 1,
		stroke: theme.palette.grey[400],
		fill: 'white',
		'&:hover': {
			stroke: theme.palette.primary.light
		}
	},
	transitionIcon: {
		pointerEvents: noneString
	}
}))

/**
 * Represents simple menu shown on state
 *
 * @param {Props} props
 * @returns
 */
function StateOverlay(props: Props) {
	const { id, show, position, size } = props
	const classes = useStyles()
	const actions = useActions(Actions, [])
	const { transitionName } = useSelector(
		({ canvas: { ghostTransition, transitionNameId } }: ReduxState) => ({
			transitionName: ghostTransition.name + transitionNameId
		}),
		[]
	)

	// Don't allow click-through
	const handleIconClick = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.nativeEvent.stopImmediatePropagation()
		},
		[]
	)

	// Toggle transition building on current state
	const handleIconMouseDown = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.preventDefault()
			e.stopPropagation()
			batch(() => {
				actions.updateGhostTransition({ startState: id })
				actions.setBuildTransition(true)
				actions.selectState('')
			})
		},
		[actions, id]
	)

	// Create new transitiion of state onto itself
	const handleIconDoubleClick = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.preventDefault()
			e.stopPropagation()
			batch(() => {
				actions.addTransition({
					name: transitionName,
					startState: id,
					endState: id
				})
				actions.useTransitionNameId()
				actions.setBuildTransition(false)
			})
		},
		[actions, id, transitionName]
	)

	if (!show) return null

	// Position of the edge of state
	const edgePosition = round(subtract(position, toPosition(0, size)))
	return (
		<g
			id="overlay"
			className={classes.root}
			transform={`translate(${edgePosition.x}, ${edgePosition.y})`}
		>
			<circle id="overlay_shadow" r={size + 3} cy={size} />
			<circle
				className={classes.transition}
				r={12}
				onMouseDown={handleIconMouseDown}
				onClick={handleIconClick}
				onDoubleClick={handleIconDoubleClick}
			/>
			<TransitionIcon
				width={24}
				height={24}
				x={-12}
				y={-12}
				color="action"
				className={classes.transitionIcon}
				alignmentBaseline="middle"
				textAnchor="middle"
			/>
		</g>
	)
}

export default StateOverlay
