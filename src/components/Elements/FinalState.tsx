import React from 'react'

import Position from '../../interfaces/Position'

interface Props {
	/** Show the element */
	show: boolean
	/** Position of base state */
	position: Position
	/** Size of base state */
	size: number
}
/**
 * Represents the outer border around final state
 *
 * @param {Props} props
 * @returns
 */
function FinalState(props: Props) {
	const { show, position, size } = props

	if (!show) return null

	return (
		<g id="final">
			<circle r={size + 4} cx={position.x} cy={position.y} />
		</g>
	)
}

export default FinalState
