import * as React from 'react'

import { Theme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/styles'

import Position from '../../interfaces/Position'
import ITransition from '../../interfaces/Transition'
import { getAngle } from '../../utils/positioning'
import { line, curve, identityCurve } from '../../utils/svg'

interface Props {
	/** Transition element data */
	data: ITransition
	/** Offset from straight line between states */
	offset: number
	/** Line start at */
	fromPoint: Position
	/** Line ends at */
	toPoint: Position
	isSelected?: boolean
	isGhost?: boolean
	/** Begins and ends at same state */
	isIdentity?: boolean
	/** There's transition coming from the to state to from state */
	hasReverseTransition?: boolean
	hasError?: boolean
	onClick?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
}

const noneString: 'none' = 'none' // Fixes linting errpr
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		cursor: 'pointer',
		'&[data-ghost="true"]': {
			pointerEvents: noneString
		},
		'&:hover > path': {
			stroke: theme.palette.primary.light
		},
		'&:hover > marker': {
			fill: theme.palette.primary.light
		}
	},
	line: {
		strokeWidth: 2,
		stroke: theme.palette.grey[400],
		fill: 'none',
		'&[data-error="true"]': {
			stroke: theme.palette.error.main
		},
		'&[data-selected="true"]': {
			stroke: theme.palette.primary.main
		}
	},
	arrowHead: {
		fill: theme.palette.grey[400],
		'&[data-error="true"]': {
			fill: theme.palette.error.main
		},
		'&[data-selected="true"]': {
			fill: theme.palette.primary.main
		}
	},
	textBckg: {
		stroke: theme.palette.grey[50],
		strokeWidth: 4,
		fontFamily: 'Roboto',
		fontSize: 16
	},
	text: {
		fill: theme.palette.text.primary,
		fontFamily: 'Roboto',
		fontSize: 16
	}
}))

// Represents transition between satte machine states
function Transition(props: Props) {
	const {
		data,
		offset,
		fromPoint,
		toPoint,
		hasError,
		isGhost,
		isSelected,
		isIdentity,
		onClick
	} = props
	const classes = useStyles()

	const id = 'transition_' + data.id

	let fromP = fromPoint
	let toP = toPoint
	let mirror = false
	const angle = getAngle(fromP, toP)
	// If the text would be upside down, flip it
	if (angle < Math.PI / 2 && angle > -Math.PI / 2) {
		mirror = true
		fromP = toPoint
		toP = fromPoint
	}

	let path: string
	if (isIdentity) {
		mirror = true // Is always flipped
		path = identityCurve(toP, fromP, offset + 1)
	} else if (offset === 0) {
		path = line(fromP, toP)
	} else {
		path = curve(fromP, toP, (mirror ? -1 : 1) * offset)
	}

	// Handle name with multiple lines
	const lines = data.name.split('\n')
	const textYPosition = React.useCallback(
		(i: number) => i * 20 - ((lines.length - 1) * 20) / 2,
		[lines]
	)
	const mapLine = React.useCallback(
		(line: string, i: number) => (
			<React.Fragment key={i}>
				<text className={classes.textBckg} dy={textYPosition(i)}>
					<textPath
						xlinkHref={'#path_' + data.id}
						startOffset="50%"
						textAnchor="middle"
						alignmentBaseline="middle"
					>
						{line}
					</textPath>
				</text>
				<text className={classes.text} dy={textYPosition(i)}>
					<textPath
						xlinkHref={'#path_' + data.id}
						startOffset={'50%'}
						textAnchor="middle"
						alignmentBaseline="middle"
					>
						{line}
					</textPath>
				</text>
			</React.Fragment>
		),
		[classes, data.id, textYPosition]
	)

	return (
		<g
			id={id}
			className={classes.root}
			onClick={onClick}
			data-ghost={isGhost}
		>
			<marker
				id={'arrowhead_' + data.id}
				orient="auto-start-reverse"
				markerWidth="3"
				markerHeight="6"
				refX="3"
				refY="3"
				data-error={hasError}
				data-selected={isSelected}
				className={classes.arrowHead}
			>
				<path d="M0,0 V6 L3,3 Z" />
			</marker>
			<path
				id={'path_' + data.id}
				className={classes.line}
				d={path}
				data-error={hasError}
				data-selected={isSelected}
				markerStart={mirror ? 'url(#arrowhead_' + data.id + ')' : ''}
				markerEnd={!mirror ? 'url(#arrowhead_' + data.id + ')' : ''}
			/>
			{lines.map(mapLine)}
		</g>
	)
}

export default React.memo(Transition, (prevProps, nextProps) => {
	if (prevProps.data.name !== nextProps.data.name) return false
	if (prevProps.offset !== nextProps.offset) return false
	if (prevProps.fromPoint.x !== nextProps.fromPoint.x) return false
	if (prevProps.fromPoint.y !== nextProps.fromPoint.y) return false
	if (prevProps.toPoint.x !== nextProps.toPoint.x) return false
	if (prevProps.toPoint.y !== nextProps.toPoint.y) return false
	if (prevProps.isSelected !== nextProps.isSelected) return false
	if (prevProps.hasError !== nextProps.hasError) return false
	if (prevProps.onClick !== nextProps.onClick) return false
	if (prevProps.hasReverseTransition !== nextProps.hasReverseTransition)
		return false
	return true
})
