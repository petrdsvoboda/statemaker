import * as React from 'react'

import CssBaseline from '@material-ui/core/CssBaseline'
import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'

import Canvas from './Canvas'
import Confirm from './Confirm'
import Errors from './Errors'
import ExploreDialog from './ExploreDialog'
import Hint from './Hint'
import Toolbar from './Toolbar'
import './App.css'

const theme = createMuiTheme({
	typography: {}
})

/**
 * App structure definition
 *
 * @returns
 */
function App() {
	return (
		<ThemeProvider theme={theme}>
			<Hint />
			<CssBaseline />
			<Confirm />
			<Errors />
			<ExploreDialog />
			<Toolbar />
			<Canvas />
		</ThemeProvider>
	)
}

export default App
