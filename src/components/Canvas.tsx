import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/styles'

import useCanvas from '../hooks/useCanvas'
import useKeyBindings from '../hooks/useKeyBindings'
import { divideByX, round } from '../interfaces/Position'
import { Actions, State as ReduxState } from '../reducers'
import GhostState from './Elements/GhostStateContainer'
import State from './Elements/StateContainer'
import GhostTransition from './Elements/GhostTransitionContainer'
import Transition from './Elements/TransitionContainer'

const useStyles = makeStyles({
	root: {
		width: '100%',
		userSelect: 'none'
	},
	grid: {
		display: 'none',
		'&[data-show="true"]': {
			display: 'block'
		}
	},
	gridPattern: {
		'& path': {
			stroke: '#bdbdbd',
			fill: 'none'
		},
		'& #smallGridPattern path': {
			strokeWidth: 0.5
		},
		'& #gridPattern path': {
			strokeWidth: 1
		}
	}
})

/**
 * SVG canvas where the state machine is being built and showed
 *
 * @returns
 */
function Canvas() {
	const actions = useActions(Actions, [])
	const classes = useStyles()
	const state = useSelector(
		(state: ReduxState) => ({
			showGrid: state.toolbar.grid,
			offset: state.canvas.offset,
			scale: state.canvas.scale,
			ghostState: state.canvas.ghostState,
			ghostTransition: state.canvas.ghostTransition,
			stateNameId: state.canvas.stateNameId,
			transitionNameId: state.canvas.transitionNameId,
			states: state.data.present.states,
			transitions: state.data.present.transitions,
			buildState: state.toolbar.buildState,
			buildTransition: state.toolbar.buildTransition,
			selectedState: !!state.canvas.selectedState,
			selectedTransition: !!state.canvas.selectedTransition
		}),
		[]
	)
	const {
		offset,
		scale,
		ghostState,
		ghostTransition,
		stateNameId,
		transitionNameId,
		states,
		transitions,
		showGrid
	} = state
	useCanvas()
	useKeyBindings()

	// Use numbers to differentiate auto-generated names
	const ghostStateName = ghostState.name + stateNameId
	const ghostTransitionName = ghostTransition.name + transitionNameId

	// Check if name is already used, otherwise increase number
	// Used mainly when importing data
	if (
		Object.values(states)
			.map(s => s.name)
			.includes(ghostStateName)
	) {
		actions.useStateNameId()
	}

	if (
		Object.values(transitions)
			.map(s => s.name)
			.includes(ghostTransitionName)
	) {
		actions.useTransitionNameId()
	}

	const gridWidth = document.body.clientWidth * 100 * -0.5
	const gridHeight = document.body.clientHeight * 100 * -0.5
	const gridTransform = `translate(${gridWidth}, ${gridHeight})`

	// Transform the svg using the zoom scale and offset
	const scaledOffset = round(divideByX(offset, scale))
	const translate = `translate(${scaledOffset.x}, ${scaledOffset.y})`
	const transform = `scale(${scale}), ${translate}`
	return (
		<svg id="canvas" className={classes.root}>
			<g transform={transform}>
				<defs className={classes.gridPattern}>
					<pattern
						id="smallGridPattern"
						width="25"
						height="25"
						patternUnits="userSpaceOnUse"
					>
						<path d="M 25 0 L 0 0 0 25" />
					</pattern>
					<pattern
						id="gridPattern"
						width="250"
						height="250"
						patternUnits="userSpaceOnUse"
					>
						<rect
							width="250"
							height="250"
							fill="url(#smallGridPattern)"
						/>
						<path d="M 250 0 L 0 0 0 250" />
					</pattern>
				</defs>
				<rect
					className={classes.grid}
					data-show={showGrid}
					transform={gridTransform}
					width="10000%"
					height="10000%"
					fill="url(#gridPattern)"
				/>
				<GhostTransition />
				{Object.values(transitions).map(t => (
					<Transition key={t.id} id={t.id} />
				))}
				<GhostState />
				{Object.keys(states).map(id => (
					<State key={id} id={id} />
				))}
			</g>
		</svg>
	)
}

export default Canvas
