import * as React from 'react'
import { useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/styles'

import { State as ReduxState } from '../reducers'

/** All text showed in main hint */
const text: string[][] = [
	['Build state', 'S'],
	['Build initial state', 'I'],
	['Build final state', 'F'],
	['Build transition', 'T'],
	['Toggle hint', 'H'],
	['Toggle grid', 'G'],
	['Select mode', 'L'],
	['Move mode', 'M'],
	['Position machine', 'P'],
	['Horizontal positioning', 'Hold Shift'],
	['Vertical positioning', 'Hold Alt'],
	['Undo', 'Ctrl + Z'],
	['Redo', 'Ctrl + Y'],
	['Save', 'Ctrl + S'],
	['Load', 'Ctrl + O']
]

/** Text showed when state is selected */
const stateText: string[][] = [
	['Set state as initial', 'Ctrl + I'],
	['Set state as final', 'Ctrl + F'],
	['Delete state', 'Ctrl + Del'],
	['New line', 'Shift + Enter'],
	['Deselect', 'Enter / Esc']
]

/** Text showed when transition is selected */
const transitionText: string[][] = [
	['Delete transition', 'Ctrl + Del'],
	['New line', 'Shift + Enter'],
	['Deselect', 'Enter / Esc']
]

/** Text showed when state is selected */
const stateBuildText: string[][] = [['Drop or click on canvas to place state']]

/** Text showed when transition is selected */
const transitionBuildText: string[][] = [['Drag or click onto state to place']]

const useStyles = makeStyles({
	root: {
		position: 'absolute',
		bottom: 0,
		right: 0,
		zIndex: 8,
		pointerEvents: 'none',
		marginRight: 8,
		marginBottom: 8,
		textAlign: 'right'
	},
	action: {
		position: 'absolute',
		top: 0,
		right: 0,
		zIndex: 8,
		pointerEvents: 'none',
		marginTop: 8,
		marginRight: 8,
		textAlign: 'right'
	},
	shortcuts: {
		display: 'grid',
		gridTemplateColumns: 'auto minmax(0, 1fr)',
		justifyItems: 'start',
		gridGap: '4px 32px',
		fontSize: '1rem',
		lineHeight: '1rem',
		marginBottom: 8,
		'&:last-child': {
			marginBottom: 0
		}
	},
	name: {
		color: 'rgba(0,0,0,0.2)',
		whiteSpace: 'pre-line',
		alignSelf: 'center'
	},
	code: {
		justifySelf: 'right',
		'& > div': {
			display: 'inline-block',
			minWidth: 40,
			textAlign: 'center',
			color: 'rgba(255,255,255)',
			backgroundColor: 'rgba(0,0,0,0.2)',
			padding: '2px 8px',
			borderRadius: 4
		}
	}
})

/**
 * Shows table with keyboard shortcuts
 *
 * @returns
 */
function Hint() {
	const classes = useStyles()
	const {
		showHint,
		buildState,
		buildTransition,
		selectedState,
		selectedTransition
	} = useSelector(
		(state: ReduxState) => ({
			showHint: state.toolbar.showHint,
			buildState: state.toolbar.buildState,
			buildTransition: state.toolbar.buildTransition,
			selectedState: state.canvas.selectedState,
			selectedTransition: state.canvas.selectedTransition
		}),
		[]
	)

	// Show hint lines with keyboard shortcuts
	const mapHint = React.useCallback(
		(row: string[], index: number) => {
			const [name, code] = row

			return (
				<React.Fragment key={index}>
					<div className={classes.name}>{name}</div>
					{code && (
						<div className={classes.code}>
							<div>{code}</div>
						</div>
					)}
				</React.Fragment>
			)
		},
		[classes]
	)

	if (!showHint) return null

	let actionText: string[][] | undefined = undefined
	if (selectedState) {
		actionText = stateText
	} else if (selectedTransition) {
		actionText = transitionText
	} else if (buildState) {
		actionText = stateBuildText
	} else if (buildTransition) {
		actionText = transitionBuildText
	}

	return (
		<React.Fragment>
			{actionText && (
				<div className={classes.action}>
					<div className={classes.shortcuts}>
						{actionText.map(mapHint)}
					</div>
				</div>
			)}
			<div className={classes.root}>
				<div className={classes.shortcuts}>{text.map(mapHint)}</div>
			</div>
		</React.Fragment>
	)
}

export default Hint
