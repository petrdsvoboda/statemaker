import React from 'react'
import { useSelector } from 'react-redux'

import { State as ReduxState } from '../reducers'
import { getErrors } from '../utils/errors'
import Error from './Error'

/**
 * Dispalys all app errors
 *
 * @returns
 */
function Errors() {
	const { canvas, data, toolbarErrors } = useSelector(
		(state: ReduxState) => ({
			canvas: state.canvas,
			data: state.data.present,
			toolbarErrors: state.toolbar.errors
		}),
		[]
	)
	const errors = [...getErrors(data, canvas), ...toolbarErrors]

	if (errors.length === 0) return null

	return (
		<React.Fragment>
			<Error errors={errors} />
		</React.Fragment>
	)
}

export default Errors
