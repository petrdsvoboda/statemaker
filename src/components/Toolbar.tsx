import * as React from 'react'

import { makeStyles } from '@material-ui/styles'

import Tools from './Toolbar/Tools'
import Input from './Toolbar/Input'

const useStyles = makeStyles({
	root: {
		position: 'absolute',
		top: 0,
		left: 0,
		zIndex: 10,
		display: 'flex',
		alignItems: 'flex-start',
		pointerEvents: 'none',
		height: '100%',
		'& *': {
			pointerEvents: 'auto'
		}
	}
})

/**
 * All toolbar elements
 *
 * @returns
 */
function Toolbar() {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<Tools />
			<Input />
		</div>
	)
}

export default Toolbar
