import * as uuid from 'uuid'

import Data from '../interfaces/Data'
import Position from '../interfaces/Position'
import IState, { StateType } from '../interfaces/State'
import ITransition from '../interfaces/Transition'
import { ActionsUnion, createAction } from '../utils/redux'
import positionForce from '../utils/positioning/force'
import positionLayer from '../utils/positioning/layered'

type PositionAlg = 'force' | 'layer'

export const Actions = {
	/** Creates new state of data of type */
	addState: (
		data: { name: string; position: Position; size: number },
		type?: StateType
	) => createAction('addState', { data, type }),
	/** Creates new transition of data */
	addTransition: (data: {
		name: string
		startState: string
		endState: string
	}) => createAction('addTransition', data),
	/** Updates state by id */
	updateState: (id: string, state: Partial<IState>) =>
		createAction('updateState', { id, state }),
	/** Updates transition by id */
	updateTransition: (id: string, transition: Partial<ITransition>) =>
		createAction('updateTransition', { id, transition }),
	/** Removes state by id */
	removeState: (id: string) => createAction('removeState', id),
	/** Removes transition by id */
	removeTransition: (id: string) => createAction('removeTransition', id),
	/** Adds or removes state from initial state array */
	toggleInitialState: (id: string) => createAction('toggleInitialState', id),
	/** Adds or removes state from final state array */
	toggleFinalState: (id: string) => createAction('toggleFinalState', id),
	/** Sets state to data */
	set: (data: Data) => createAction('set', data),
	/** Sets positions of all states */
	position: (type: PositionAlg) => createAction('position', type),
	/** Resets state to initial */
	delete: () => createAction('delete')
}

export type Actions = ActionsUnion<typeof Actions>

export interface State extends Data {}

export const initialState: State = {
	initialStates: [],
	finalStates: [],
	states: {},
	transitions: {}
}

function reducer(state = initialState, action: Actions): State {
	switch (action.type) {
		case 'addState': {
			const id = uuid.v4()
			const initialStates =
				action.payload.type === 'initial'
					? [...state.initialStates, id]
					: state.initialStates
			const finalStates =
				action.payload.type === 'final'
					? [...state.finalStates, id]
					: state.finalStates
			return {
				...state,
				initialStates,
				finalStates,
				states: {
					...state.states,
					[id]: {
						...action.payload.data,
						id
					}
				}
			}
		}
		case 'addTransition': {
			const id = uuid.v4()
			return {
				...state,
				transitions: {
					...state.transitions,
					[id]: {
						...action.payload,
						id
					}
				}
			}
		}
		case 'updateState': {
			const { id, state: s } = action.payload
			if (!state.states.hasOwnProperty(id)) return state

			return {
				...state,
				states: {
					...state.states,
					[id]: {
						...state.states[id],
						...s
					}
				}
			}
		}
		case 'updateTransition': {
			const { id, transition } = action.payload
			if (!state.transitions.hasOwnProperty(id)) return state

			return {
				...state,
				transitions: {
					...state.transitions,
					[id]: {
						...state.transitions[id],
						...transition
					}
				}
			}
		}
		case 'removeState': {
			const id = action.payload
			if (!state.states.hasOwnProperty(id)) return state
			const { [id]: _, ...states } = state.states
			return {
				...state,
				states,
				initialStates: state.initialStates.filter(s => s !== id),
				finalStates: state.finalStates.filter(s => s !== id),
				transitions: Object.values(state.transitions)
					.filter(t => ![t.startState, t.endState].includes(id))
					.reduce((acc, cur) => ({ ...acc, [cur.id]: cur }), {})
			}
		}
		case 'removeTransition': {
			const id = action.payload
			if (!state.transitions.hasOwnProperty(id)) return state
			const { [id]: _, ...transitions } = state.transitions
			return {
				...state,
				transitions
			}
		}
		case 'toggleInitialState': {
			const id = action.payload
			const states = state.initialStates
			const initialStates = states.includes(id)
				? states.filter(s => s !== id)
				: [...states, id]
			return {
				...state,
				initialStates
			}
		}
		case 'toggleFinalState': {
			const id = action.payload
			const states = state.finalStates
			const finalStates = states.includes(id)
				? states.filter(s => s !== id)
				: [...states, id]
			return {
				...state,
				finalStates
			}
		}
		case 'set': {
			return {
				...initialState,
				...action.payload
			}
		}
		case 'position': {
			let alg = positionLayer
			if (action.payload === 'force') alg = positionForce
			return alg(state)
		}
		case 'delete': {
			return initialState
		}
		default:
			return state
	}
}

export default reducer
