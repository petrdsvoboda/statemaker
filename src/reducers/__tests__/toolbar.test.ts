import reducer, { initialState, Actions, State } from '../toolbar'

describe('toolbar reducer', () => {
	it('sets input', () => {
		expect(reducer(initialState, Actions.setInput('x'))).toEqual({
			...initialState,
			input: 'x'
		} as State)
	})

	it('sets algorithm', () => {
		expect(reducer(initialState, Actions.setAlgorithm('force'))).toEqual({
			...initialState,
			algorithm: 'force'
		} as State)
	})

	it('resets input', () => {
		expect(reducer(initialState, Actions.resetInput())).toEqual(
			initialState
		)
	})

	it('adds error', () => {
		expect(
			reducer(
				{
					...initialState,
					errors: ['x']
				},
				Actions.addError('err')
			)
		).toEqual({
			...initialState,
			errors: ['x', 'err']
		} as State)
	})

	it('sets errors', () => {
		expect(
			reducer(
				{
					...initialState,
					errors: ['x']
				},
				Actions.setErrors(['err1', 'err2'])
			)
		).toEqual({
			...initialState,
			errors: ['err1', 'err2']
		} as State)
	})

	it('resets errors', () => {
		expect(
			reducer(
				{
					...initialState,
					errors: ['x']
				},
				Actions.resetErrors()
			)
		).toEqual(initialState)
	})

	it('toggles build state', () => {
		expect(reducer(initialState, Actions.toggleBuildState())).toEqual({
			...initialState,
			buildState: true
		} as State)
		expect(
			reducer(initialState, Actions.toggleBuildState('initial'))
		).toEqual({
			...initialState,
			buildState: true,
			buildStateType: 'initial'
		} as State)
		expect(
			reducer(initialState, Actions.toggleBuildState('final'))
		).toEqual({
			...initialState,
			buildState: true,
			buildStateType: 'final'
		} as State)
	})

	it('toggles build transition', () => {
		expect(reducer(initialState, Actions.toggleBuildTransition())).toEqual({
			...initialState,
			buildTransition: true
		} as State)
	})

	it('sets build state', () => {
		expect(reducer(initialState, Actions.setBuildState(true))).toEqual({
			...initialState,
			buildState: true
		} as State)
	})

	it('sets build transition', () => {
		expect(reducer(initialState, Actions.setBuildTransition(true))).toEqual(
			{
				...initialState,
				buildTransition: true
			} as State
		)
	})

	it('toggles hint', () => {
		expect(reducer(initialState, Actions.toggleShowHint())).toEqual({
			...initialState,
			showHint: false
		} as State)
	})

	it('sets explore', () => {
		expect(reducer(initialState, Actions.setShowExplore(true))).toEqual({
			...initialState,
			showExplore: true
		} as State)
	})

	it('resets', () => {
		expect(reducer(initialState, Actions.reset())).toEqual({
			...initialState,
			buildState: false,
			buildTransition: false,
			input: '',
			errors: []
		} as State)
	})

	it('sets confirm text', () => {
		expect(reducer(initialState, Actions.setConfirmText('x'))).toEqual({
			...initialState,
			confirmText: 'x'
		} as State)
	})

	it('sets mouse mode', () => {
		expect(
			reducer(initialState, Actions.setMouseMode('horizontal'))
		).toEqual({
			...initialState,
			mouseMode: 'horizontal'
		} as State)
	})

	it('toggles grid', () => {
		expect(reducer(initialState, Actions.toggleGrid())).toEqual({
			...initialState,
			grid: !initialState.grid
		} as State)
	})
})
