import reducer, { initialState, Actions, State } from '../canvas'

describe('canvas reducer', () => {
	it('sets cursor', () => {
		expect(
			reducer(initialState, Actions.setCursor({ x: 10, y: 10 }))
		).toEqual({
			...initialState,
			cursor: { x: 10, y: 10 }
		} as State)
	})

	it('sets offset', () => {
		expect(
			reducer(initialState, Actions.setOffset({ x: 10, y: 10 }))
		).toEqual({
			...initialState,
			offset: { x: 10, y: 10 }
		} as State)
	})

	it('sets cursor mode', () => {
		expect(reducer(initialState, Actions.setCursorMode('select'))).toEqual({
			...initialState,
			cursorMode: 'select'
		} as State)
	})

	it('sets positioning state', () => {
		expect(reducer(initialState, Actions.setPositioningState('x'))).toEqual(
			{
				...initialState,
				positioningState: 'x'
			} as State
		)
	})

	it('correctly zooms', () => {
		expect(
			reducer(initialState, Actions.zoom(0.1, { x: 100, y: 100 }))
		).toEqual({
			...initialState,
			scale: 1.1,
			offset: { x: -10, y: -10 }
		} as State)
	})

	it('correctly zooms to center', () => {
		expect(reducer(initialState, Actions.zoom(0.1))).toEqual({
			...initialState,
			scale: 1.1,
			offset: { x: 0, y: 0 }
		} as State)
	})

	it("doesn't zoom over 3", () => {
		expect(
			reducer(initialState, Actions.zoom(2.1, { x: 100, y: 100 }))
		).toEqual(initialState)
	})

	it("doesn't zoom under 0.15", () => {
		expect(
			reducer(initialState, Actions.zoom(-0.86, { x: 100, y: 100 }))
		).toEqual(initialState)
	})

	it('correctly deselects', () => {
		expect(reducer(initialState, Actions.deselect())).toEqual({
			...initialState,
			selectedState: '',
			selectedTransition: '',
			ghostTransition: {
				...initialState.ghostTransition,
				startState: '',
				endState: ''
			}
		} as State)
	})

	it('correctly updates ghost state', () => {
		expect(
			reducer(initialState, Actions.updateGhostState({ size: 25 }))
		).toEqual({
			...initialState,
			ghostState: {
				...initialState.ghostState,
				size: 25
			}
		} as State)
	})

	it('correctly updates ghost transition', () => {
		expect(
			reducer(
				initialState,
				Actions.updateGhostTransition({ startState: 'x' })
			)
		).toEqual({
			...initialState,
			ghostTransition: {
				...initialState.ghostTransition,
				startState: 'x'
			}
		} as State)
	})

	it('correctly selects state', () => {
		expect(reducer(initialState, Actions.selectState('x'))).toEqual({
			...initialState,
			selectedState: 'x'
		} as State)
	})

	it('correctly deselects state', () => {
		expect(
			reducer(
				{
					...initialState,
					selectedState: 'x'
				},
				Actions.selectState('x')
			)
		).toEqual({
			...initialState,
			selectedState: ''
		} as State)
	})

	it('correctly selects transition', () => {
		expect(reducer(initialState, Actions.selectTransition('x'))).toEqual({
			...initialState,
			selectedTransition: 'x'
		} as State)
	})

	it('correctly deselects transition', () => {
		expect(
			reducer(
				{
					...initialState,
					selectedTransition: 'x'
				},
				Actions.selectTransition('x')
			)
		).toEqual({
			...initialState,
			selectedTransition: ''
		} as State)
	})

	it('correctly increases state id', () => {
		expect(reducer(initialState, Actions.useStateNameId())).toEqual({
			...initialState,
			stateNameId: 2
		} as State)
	})

	it('correctly increases transition id', () => {
		expect(reducer(initialState, Actions.useTransitionNameId())).toEqual({
			...initialState,
			transitionNameId: 2
		} as State)
	})
})
