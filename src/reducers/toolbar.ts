import { StateType } from '../interfaces/State'
import { ActionsUnion, createAction } from '../utils/redux'

type Algorithm = 'force' | 'layer'
type MouseMode = 'vertical' | 'horizontal' | 'default'

export const Actions = {
	/** Sets input to text */
	setInput: (text: string) => createAction('setInput', text),
	/** Sets positioning alg type */
	setAlgorithm: (value: Algorithm) => createAction('setAlgorithm', value),
	/** Sets input to '' */
	resetInput: () => createAction('resetInput'),
	/** Add error to errors */
	addError: (error: string) => createAction('addError', error),
	/** Assign error array */
	setErrors: (errors: string[]) => createAction('setErrors', errors),
	/** Deletes errors to empty arr */
	resetErrors: () => createAction('resetErrors'),
	/** Toggle build state mode */
	toggleBuildState: (stateType?: StateType) =>
		createAction('toggleBuildState', stateType),
	/** Toggle build transition mode */
	toggleBuildTransition: () => createAction('toggleBuildTransition'),
	/** Sets build state mode to value */
	setBuildState: (value: boolean) => createAction('setBuildState', value),
	/** Sets build transition mode to value */
	setBuildTransition: (value: boolean) =>
		createAction('setBuildTransition', value),
	/** Toggle hint to show */
	toggleShowHint: () => createAction('toggleShowHint'),
	/** Toggle explore to show */
	setShowExplore: (value: boolean) => createAction('setShowExplore', value),
	/** Resets input and build modes */
	reset: () => createAction('reset'),
	/** Set info snackbar text */
	setConfirmText: (value: string) => createAction('setConfirmText', value),
	/** Show/hide grid */
	toggleGrid: () => createAction('toggleGrid'),
	/** Sets vertical or horizontal only mouse move mode */
	setMouseMode: (value: MouseMode) => createAction('setMouseMode', value)
}

export type Actions = ActionsUnion<typeof Actions>

export interface State {
	/** Text input value */
	input: string
	/** Displayed errors */
	errors: string[]
	/** Build state mode toggle */
	buildState: boolean
	/** Build state mode state type */
	buildStateType?: StateType
	/** Build transition mode toggle */
	buildTransition: boolean
	/** Show/hide hint */
	showHint: boolean
	/** Show/hide explore */
	showExplore: boolean
	/** Chosen positioning alg */
	algorithm: Algorithm
	/** Info snackbar text */
	confirmText: string
	/** Show/hide grid */
	grid: boolean
	/** Default/horizontal.vertical mouse mode */
	mouseMode: MouseMode
}

export const initialState: State = {
	input: '',
	errors: [],
	buildState: false,
	buildTransition: false,
	showHint: true,
	showExplore: false,
	algorithm: 'layer',
	confirmText: '',
	grid: false,
	mouseMode: 'default'
}

function reducer(state = initialState, action: Actions): State {
	switch (action.type) {
		case 'setInput': {
			return {
				...state,
				input: action.payload
			}
		}
		case 'setAlgorithm': {
			return {
				...state,
				algorithm: action.payload
			}
		}
		case 'resetInput': {
			return {
				...state,
				input: ''
			}
		}
		case 'addError': {
			return {
				...state,
				errors: [...state.errors, action.payload]
			}
		}
		case 'setErrors': {
			return {
				...state,
				errors: action.payload
			}
		}
		case 'resetErrors': {
			return {
				...state,
				errors: []
			}
		}
		case 'toggleBuildState': {
			return {
				...state,
				buildState: !state.buildState,
				buildStateType: action.payload
			}
		}
		case 'toggleBuildTransition': {
			return {
				...state,
				buildTransition: !state.buildTransition
			}
		}
		case 'setBuildState': {
			return {
				...state,
				buildState: action.payload
			}
		}
		case 'setBuildTransition': {
			return {
				...state,
				buildTransition: action.payload
			}
		}
		case 'toggleShowHint': {
			return {
				...state,
				showHint: !state.showHint
			}
		}
		case 'setShowExplore': {
			return {
				...state,
				showExplore: action.payload
			}
		}
		case 'reset': {
			return {
				...state,
				buildState: false,
				buildTransition: false,
				input: '',
				errors: []
			}
		}
		case 'setConfirmText': {
			return {
				...state,
				confirmText: action.payload
			}
		}
		case 'setMouseMode': {
			return {
				...state,
				mouseMode: action.payload
			}
		}
		case 'toggleGrid': {
			return {
				...state,
				grid: !state.grid
			}
		}
		default:
			return state
	}
}

export default reducer
