import * as React from 'react'
import { batch, useActions, useSelector, useDispatch } from 'react-redux'
import { ActionCreators } from 'redux-undo'

import { saveState, loadState } from '../utils/persist'
import { Actions, State } from '../reducers'

/**
 * Manages all global keyboard events and mouse events
 *
 */
const useKeyBindings = () => {
	const dispatch = useDispatch()
	const actions = useActions(Actions, [])

	const state = useSelector(
		(state: State) => ({
			algorithm: state.toolbar.algorithm,
			mouseMode: state.toolbar.mouseMode,
			selectedState: state.canvas.selectedState,
			selectedTransition: state.canvas.selectedTransition,
			buildState: state.toolbar.buildState,
			buildTransition: state.toolbar.buildTransition,
			data: state.data.present
		}),
		[]
	)

	const {
		algorithm,
		mouseMode,
		selectedState,
		selectedTransition,
		buildState,
		buildTransition,
		data
	} = state

	// Delete selected elements
	const handleDelete = React.useCallback(() => {
		if (selectedState) {
			batch(() => {
				actions.deselect()
				actions.removeState(selectedState)
			})
		} else if (selectedTransition) {
			batch(() => {
				actions.deselect()
				actions.removeTransition(selectedTransition)
			})
		}
	}, [actions, selectedState, selectedTransition])

	// Reset selection
	const handleDeselect = React.useCallback(() => {
		batch(() => {
			actions.deselect()
			actions.reset()
		})
	}, [actions])

	// Identifier if there is ongoing action on canvas
	const ongoingAction =
		selectedState || selectedTransition || buildState || buildTransition
	// Toogle build modes when there's no ongoing action
	const handleBuild = React.useCallback(
		(
			variant: 'default' | 'initial' | 'final' | 'transition',
			e: KeyboardEvent
		) => {
			if (ongoingAction) return
			e.preventDefault()

			if (variant === 'transition') {
				actions.toggleBuildTransition()
			} else {
				actions.toggleBuildState(variant)
			}
		},
		[actions, ongoingAction]
	)

	// Loads data from local storage
	const handleLoad = React.useCallback(() => {
		const data = loadState()
		if (data) {
			batch(() => {
				actions.set(data)
				actions.setConfirmText('Automata loaded')
			})
		}
	}, [actions])

	// Saves data to local storage
	const handleSave = React.useCallback(() => {
		saveState(data)
		actions.setConfirmText('Automata saved')
	}, [actions, data])

	// Handle key events
	const handleKey = React.useCallback(
		(e: KeyboardEvent) => {
			switch (e.key) {
				case 'Delete':
					if (e.ctrlKey) handleDelete()
					break
				case 'Escape':
				case 'Enter':
					handleDeselect()
					break
				case 's':
					if (e.ctrlKey) {
						e.preventDefault()
						handleSave()
					} else {
						handleBuild('default', e)
					}
					break
				case 'i':
					// Sets slected state as initial
					if (e.ctrlKey && selectedState)
						actions.toggleInitialState(selectedState)
					else handleBuild('initial', e)
					break
				case 'f':
					// Sets slected state as final
					if (e.ctrlKey && selectedState) {
						e.preventDefault()
						actions.toggleFinalState(selectedState)
					} else {
						handleBuild('final', e)
					}
					break
				case 't':
					handleBuild('transition', e)
					break
				case 'h':
					e.preventDefault()
					actions.toggleShowHint()
					break
				case 'm':
					e.preventDefault()
					batch(() => {
						actions.deselect()
						actions.setCursorMode('move')
					})
					break
				case 'l':
					e.preventDefault()
					actions.setCursorMode('select')
					break
				case 'p':
					e.preventDefault()
					actions.position(algorithm)
					break
				case 'g':
					e.preventDefault()
					actions.toggleGrid()
					break
				case 'Shift':
					e.preventDefault()
					actions.setMouseMode('horizontal')
					break
				case 'Alt':
					e.preventDefault()
					actions.setMouseMode('vertical')
					break
				case 'z':
					if (e.ctrlKey) dispatch(ActionCreators.undo())
					break
				case 'y':
					if (e.ctrlKey) dispatch(ActionCreators.redo())
					break
				case 'o':
					if (e.ctrlKey) {
						e.preventDefault()
						handleLoad()
					}
					break
				default:
			}
		},
		[
			actions,
			dispatch,
			algorithm,
			handleDelete,
			handleDeselect,
			handleBuild,
			handleLoad,
			handleSave,
			selectedState
		]
	)

	// Disable vertical and horizontal mouse modes
	const handleKeyUp = React.useCallback(
		(e: KeyboardEvent) => {
			if (mouseMode !== 'default') {
				actions.setMouseMode('default')
			}
		},
		[actions, mouseMode]
	)

	// Disallow right click
	const handleContextMenu = React.useCallback(
		(e: MouseEvent) => {
			e.preventDefault()
			e.stopPropagation()
			handleDeselect()
		},
		[handleDeselect]
	)

	// Disallow browser zoom
	const handleWheel = React.useCallback((e: MouseEvent) => {
		if (e.ctrlKey) {
			e.preventDefault()
		}
	}, [])

	React.useLayoutEffect(() => {
		document.addEventListener('keydown', handleKey)
		document.addEventListener('keyup', handleKeyUp)
		document.addEventListener('contextmenu', handleContextMenu)
		document.addEventListener('wheel', handleWheel, { passive: false })
		document.addEventListener('click', handleDeselect)
		return () => {
			document.removeEventListener('keydown', handleKey)
			document.removeEventListener('keyup', handleKeyUp)
			document.removeEventListener('contextmenu', handleContextMenu)
			document.removeEventListener('wheel', handleWheel)
			document.removeEventListener('click', handleDeselect)
		}
	}, [handleKey, handleKeyUp, handleContextMenu, handleDeselect, handleWheel])
}

export default useKeyBindings
