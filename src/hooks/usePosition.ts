import * as React from 'react'
import { useSelector } from 'react-redux'

import useCursor from '../hooks/useCursor'
import Position, { round, normalize } from '../interfaces/Position'
import { State as ReduxState } from '../reducers'

/**
 * Cursor position relative to application scale and offset
 *
 * @param {boolean} allowChange
 * @returns {Position}
 */
const usePosition = (allowChange: boolean): Position => {
	const { offset, scale } = useSelector(
		(state: ReduxState) => ({
			offset: state.canvas.offset,
			scale: state.canvas.scale
		}),
		[]
	)
	const update = allowChange
	// Get cursor position if the indicator is true
	const cursor = useCursor(update)
	const [position, setPosition] = React.useState<Position>(cursor)

	React.useLayoutEffect(() => {
		if (!update) return

		// Normalize the cursor position
		setPosition(normalize(cursor, offset, scale))
	}, [update, cursor, scale, offset])

	return round(position)
}

export default usePosition
