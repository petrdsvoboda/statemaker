import * as React from 'react'
import { useSelector } from 'react-redux'

import Position, { toPosition, snap, add, modX } from '../interfaces/Position'
import { State as ReduxState } from '../reducers'

const GRID_SIZE = 25

/**
 * Current cursor position on document
 * Snappes to grid and uses mouse modes
 *
 * @param {boolean} shouldMove
 * @returns {Position}
 */
const useCursor = (shouldMove: boolean): Position => {
	const { mouseMode, gridOn, scale, offset } = useSelector(
		(state: ReduxState) => ({
			mouseMode: state.toolbar.mouseMode,
			gridOn: state.toolbar.grid,
			scale: state.canvas.scale,
			offset: state.canvas.offset
		}),
		[]
	)
	const [position, setPosition] = React.useState<Position>(
		toPosition(window.clientX, window.clientY)
	)

	// Calculates current mouse position
	const handleSetPosition = React.useCallback(
		(original: Position) => (e: MouseEvent) => {
			let current = toPosition(original.x, original.y)
			// Can move only horizontally or vertically
			if (mouseMode === 'horizontal') {
				current.x = e.clientX
			} else if (mouseMode === 'vertical') {
				current.y = e.clientY
			} else {
				current = toPosition(e.clientX, e.clientY)
			}

			// Snap to grid
			if (gridOn) {
				current = add(
					snap(current, GRID_SIZE * scale),
					modX(offset, GRID_SIZE * scale)
				)
			}

			setPosition(current)
		},
		[mouseMode, gridOn, scale, offset]
	)

	React.useLayoutEffect(() => {
		// Update only when should move
		if (!shouldMove) return

		// Initial position
		let current = toPosition(window.clientX, window.clientY)
		if (gridOn) {
			current = snap(current, GRID_SIZE)
		}
		setPosition(current)
		// Use handler for move event
		const onSetPosition = handleSetPosition(current)

		document.addEventListener('mousemove', onSetPosition)
		return () => {
			document.removeEventListener('mousemove', onSetPosition)
		}
	}, [handleSetPosition, shouldMove, gridOn])

	return position
}

export default useCursor
