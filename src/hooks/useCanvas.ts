import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import { toPosition, add } from '../interfaces/Position'
import { Actions, State } from '../reducers'

/**
 * Handles SVG canvas zoom, move and cursor modes
 *
 */
const useCanvas = () => {
	const [isMoving, setMoving] = React.useState(false)
	const actions = useActions(Actions, [])

	const { offset, cursorMode } = useSelector(
		(state: State) => ({
			offset: state.canvas.offset,
			cursorMode: state.canvas.cursorMode
		}),
		[]
	)

	// Handles moving offset
	const handleSetPosition = React.useCallback(
		(e: MouseEvent) => {
			if (isMoving) {
				const mouse = toPosition(e.movementX, e.movementY)
				actions.setOffset(add(offset, mouse))
			}
		},
		[actions, isMoving, offset]
	)

	// Decides when moving is allowed
	const handleSetMoving = React.useCallback(
		(e: MouseEvent) => {
			// Move on middle mouse or left mouse when in move mode
			if (e.which === 2 || (cursorMode === 'move' && e.which === 1)) {
				setMoving(true)
			}
		},
		[cursorMode]
	)

	const handleUnsetMoving = React.useCallback((e: MouseEvent) => {
		setMoving(false)
	}, [])

	// Zoom on mouse position
	const handleWheel = React.useCallback(
		(e: WheelEvent) => {
			const mouse = toPosition(e.clientX, e.clientY)
			const factor = 0.05 * Math.sign(e.deltaY)

			actions.zoom(factor, mouse)
		},
		[actions]
	)

	React.useLayoutEffect(() => {
		document.addEventListener('mousemove', handleSetPosition)
		document.addEventListener('mousedown', handleSetMoving)
		document.addEventListener('mouseup', handleUnsetMoving)
		document.addEventListener('wheel', handleWheel)
		return () => {
			document.removeEventListener('mousemove', handleSetPosition)
			document.removeEventListener('mousedown', handleSetMoving)
			document.removeEventListener('mouseup', handleUnsetMoving)
			document.removeEventListener('wheel', handleWheel)
		}
	}, [handleSetPosition, handleSetMoving, handleUnsetMoving, handleWheel])
}

export default useCanvas
