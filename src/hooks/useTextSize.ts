import * as React from 'react'

import IState from '../interfaces/State'
import { getSizeFromRef } from '../utils/size'

/**
 * Updates state's size to fit current text content
 *
 * @param {string} text
 * @param {(state: Partial<IState>) => void} onUpdate
 * @param {boolean} [runOnInit=true]
 * @returns {React.RefObject<SVGTextElement>}
 */
const useTextSize = (
	text: string,
	onUpdate: (state: Partial<IState>) => void,
	runOnInit = true
): React.RefObject<SVGTextElement> => {
	const [ref] = React.useState(React.createRef<SVGTextElement>())
	const [prevText, setPrevText] = React.useState(text)

	const firstUpdate = React.useRef(true) // Checks for 1st render
	const refContent = ref ? ref.current : ''
	React.useLayoutEffect(() => {
		// Don't update on first render
		if (firstUpdate.current && !runOnInit) {
			firstUpdate.current = false
			return
		}
		// Don't update if no text or text hasn't changed
		if (!refContent || (prevText === text && firstUpdate.current !== true))
			return

		setPrevText(text)
		const size = getSizeFromRef(refContent)
		onUpdate({ size }) // Dispatch redux action to update state
	}, [runOnInit, refContent, text, prevText, onUpdate])

	return ref
}

export default useTextSize
